﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace fsb.smartcontract.Controllers
{
    public class DistrictController : BaseController
    {
        private readonly ILogger<DistrictController> _logger;
        private readonly IToastNotification _toastNotification;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public DistrictController(
            ILogger<DistrictController> logger,
            IHttpClientFactory factory,
            IToastNotification toastNotification,
            IHttpContextAccessor httpContextAccessor
            ) : base(factory)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _httpContextAccessor = httpContextAccessor;
        }

        public IActionResult Districts()
        {
            return View();
        }
    }
}
