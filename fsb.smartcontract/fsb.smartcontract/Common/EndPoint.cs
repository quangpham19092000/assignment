﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontract.Common
{
    public class Path
    {
        //Account
        public static string ACCOUNT_LOGIN = "api/Account/Login";
        public static string ACCOUNT_CREATE = "api/account/create";
        public static string ACCOUNT_UPDATE = "api/account/update";
        public static string ACCOUNT_DELETE = "api/account/delete";
        public static string ACCOUNT_TABLE = "api/account/userTable";
        public static string AUTHENCATE_LOGIN = "api/Account/Authencate";
        public static string AUTHENCATEAZUREAD_LOGIN = "api/Account/AuthencateAzureAD";
        public static string CONFIG = "api/Account/getConfig";

        //UserInfo
        public static string USER_DATATABLE = "api/UserInfo/GetListUser";
        public static string USER_CREATE = "api/UserInfo/CreateUser";
        public static string USER_BYID = "api/UserInfo/GetUserById";
        public static string USER_UPDATE = "api/UserInfo/UpdateUser";
        public static string USER_DELETE = "api/UserInfo/DeleteUser";
        public static string USER_BYUSERNAME = "api/UserInfo/GetUserByUserName";
        public static string USER_CHANGEPASSWORD = "api/UserInfo/SaveChangePassword";
        public static string USERORG_BYID = "api/UserInfo/GetUserOrganizationById";
        public static string USER_FORCOMBO = "api/UserInfo/GetListUserForCombo";
        public static string USER_GETALL = "api/UserInfo/GetAllUser";
        public static string USER_EXPORT = "api/UserInfo/GetDataExportUserInfo";

        //Role
        public static string ROLE_FORCOMBO = "api/Role/GetListRoleForCombo";

        //Company
        public static string COMPANY_DATATABLE = "api/Company/GetListCompany";
        public static string COMPANY_BYID = "api/Company/GetCompanyById";
        public static string COMPANY_CREATE = "api/Company/CreateCompany";
        public static string COMPANY_UPDATE = "api/Company/UpdateCompany";
        public static string COMPANY_DELETE = "api/Company/DeleteCompany";

        //Role
        public static string ROLE_DATATABLE = "api/Role/GetListRole";
        public static string ROLE_CREATE = "api/Role/CreateRole";
        public static string ROLE_DELETE = "api/Role/DeleteRole";
        public static string ROLE_BYID = "api/Role/GetRoleById";
        public static string ROLE_UPDATE = "api/Role/UpdateRole";
    }
}
